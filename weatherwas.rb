require 'forecast_io'
require 'time'
require 'Open3'

ForecastIO.api_key = File.read("API_KEY").strip
lat = 42.922
lng = -89.395
yesterday = Time.parse((Date.today - 1).strftime("%Y-%m-%d 23:59:59"))

forecast = ForecastIO.forecast(lat, lng, time: yesterday.to_i, params: { units: 'si'})
count = 0
temp = 0
forecast.hourly.data.each { |h|
		temp=temp+h['temperature']
		count=count+1
	}

dp = forecast.daily.data[0]
max = dp[:temperatureMax]
min = dp[:temperatureMin]
minTime = Time.at(dp[:temperatureMinTime])
maxTime = Time.at(dp[:temperatureMaxTime])
output = [
	"**Low**: %d at %s" % [min.round(), minTime.strftime("%H:%M")],
	"**High**: %d at %s" % [max.round(), maxTime.strftime("%H:%M")]
]
stdin, stdout, stderr = Open3.popen3("/usr/local/bin/dayone -d='#{yesterday.strftime('%Y-%m-%d %H:%M:%S %z')}' new")
stdin.puts "## Daily Weather Summary\n\n"
if dp[:temperatureMinTime] > dp[:temperatureMaxTime] then
	stdin.puts output[1]
	stdin.puts output[0]
else
	stdin.puts output[0]
	stdin.puts output[1]
end
stdin.puts "**Average**: %d" % [(temp/count).round()]
stdin.puts "\n\n#weather"
stdin.close()

