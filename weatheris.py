import forecastio
from datetime import datetime,timedelta

fh = open("API_KEY","r")
api_key = fh.readline()
fh.close()
lat = 42.922
lng = -89.395

forecast = forecastio.load_forecast(api_key, lat, lng, units="si")
dp = forecast.daily().data[0]
max = dp.temperatureMax
min = dp.temperatureMin
minTime = datetime.fromtimestamp(dp.temperatureMinTime)
maxTime = datetime.fromtimestamp(dp.temperatureMaxTime)
print "Low: %d at %s" % (min, minTime.strftime("%H:%M"))
print "High: %d at %s" % (max, maxTime.strftime("%H:%M"))
