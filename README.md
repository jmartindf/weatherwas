# Weather Was

## Purpose

Look up the weather for the given day (defaults to today) and show the observed high and low temperature.

## Implementations

### Python

    mkvirtualenv weatherwas
    workon weatherwas
    pip install -r requirements.txt
    deactivate

weatherwas.py–Show the high and low for yesterday.

weatheris.py–Show the high and low for today (so far).

### Ruby

    rvm gemset create weatherwas
    rvm use 1.9.3@weatherwas
    gem install forecast_io
    bundle install
    rvm use system
    rvm alias create weatherwas ruby-1.9.3-p194@weatherwas
    ~/.rvm/wrappers/weatherwas/ruby weatherwas.rb

## ToDo

- Automatically look up latitude and longitude for the current location
- Automatically get current date / end of day
- Convert to SLogger plugin
- Use config for API key
- Use config for units

## Tools

<https://developer.forecast.io>    
<https://github.com/darkskyapp/forecast-ruby>    
<https://github.com/ZeevG/python-forcast.io>    
