#! /bin/bash
PYTHON="/Users/jmartin/.virtualenvs/weatherwas/bin/python"
cd /Users/jmartin/development/weatherwas
TMP_FILE=`mktemp /tmp/weatheris.XXXXXX` || exit 1
$PYTHON weatheris.py > $TMP_FILE

osascript <<EOF
tell application "LaunchBar"
    display in large type (do shell script("cat $TMP_FILE"))
end tell
EOF

rm $TMP_FILE
